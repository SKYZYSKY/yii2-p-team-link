Виджет ссылки на сайт p-team 
=============================
Виджет ссылки на сайт p-team 

Installation
------------

The preferred way to install this extension is through [composer](https://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist p-team/yii2-p-team-link "*"
```

or add

```
"p-team/yii2-p-team-link": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \p_team\widgets\p_team_link\AutoloadExample::widget(); ?>```
