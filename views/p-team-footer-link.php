<?php

/* 
* @Author SKYZYSKY (yanchevdm@gmail.com)
* @link https://p-team.ru
* @license
* В любом виде и любыми средствами запрещено копирование и/или распростронение
* Использование данного ПО возможно только с согласия автора.
 */

?>
<style>
    #pTeamFooterLink{
        cursor: pointer;
        color: inherit;
        text-decoration: inherit;
    }
    @keyframes heartbeat
    {
        0%
        {
            transform: scale( 1 )
        }
        30%
        {
            transform: scale( 1.2 )
        }
    }
    #devHeart {
        color: #8e2828;
    }
    #devHeart::before {
        animation: heartbeat 1s infinite;
    }
</style>
<a id="pTeamFooterLink" href="https://p-team.ru" target="_blank">
    Developed with <i id="devHeart" class="bi bi-heart-fill"></i> by PerfectTeam</p>
</a>