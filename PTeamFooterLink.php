<?php
/* 
* @Author SKYZYSKY (yanchevdm@gmail.com)
* @link https://p-team.ru
* @license
* В любом виде и любыми средствами запрещено копирование и/или распростронение
* Использование данного ПО возможно только с согласия автора.
 */

namespace p_team\widgets\p_team_link;

use Yii;

class PTeamFooterLink extends \yii\bootstrap5\Widget
{
    


    /**
     * {@inheritdoc}
     */
    public function run()
    {
        
        echo $this->render('p-team-footer-link');
    }
}
